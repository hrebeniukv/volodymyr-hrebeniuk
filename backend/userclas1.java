public class userclas1 extends userclass {


    private double salary;
    private double workExperience;



    public userclas1(String firstName, String lastName, String email,String password,
                     String mobilPhone, String phone,double salary,double workExperience) {
        super(firstName, lastName, email, password, mobilPhone, phone);
        this.salary = salary;
        this.workExperience = workExperience;
    }

    //Конструктор для зарплаты и стажа
    public userclas1( String email,String password, double salary,double workExperience) {
        super( email, password);
        this.salary = salary;
        this.workExperience = workExperience;
    }



    //Увеличение зарплаты
    private int changesalaru() {
        if (workExperience < 2) {
            return 1;
        }else if (workExperience >5){
            return 3;
        }else {
            return 2;
        }
    }


    public double changedsalary(){
        switch (changesalaru()){
            case 1:
                return  (salary*1.05);
            case 3:
                return  (salary*1.15);
            case 2:
                return (salary*1.10);

        }
        return 0;
    }
}




