public class userclass {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String mobilPhone;
    private String phone;

    //Конструктор для всех полей
    public userclass(String firstName, String lastName, String email,
                     String password, String mobilPhone, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email(email);
        this.password = password(password);
        this.mobilPhone = mobilPhone;
        this.phone = phone;


    }


    //Конструктор для емейл и пароля
    public userclass(String email,String password) {
        this.email =  email(email);
        this.password = password(password);

    }

    private String password(String password) {
        if (password.length() < 8 || password.length() > 16) {
            throw new IllegalArgumentException("password is wrong");
        } else {
            return password;
        }
    }

    private String email(String email) {
        for (int i = 0; i < email.length(); i++) {
            if (email.charAt(i) == 64) {
                return password;
            }
        }
        throw new IllegalArgumentException("email is wrong");

    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setMobilPhone(String mobilPhone) {
        this.mobilPhone = mobilPhone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }



    public String getFirstName(){
        return this.firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public String getEmail(){
        return email;
    }
    public String getPassword(){
        return password;
    }
    public String getMobilPhone(){
        return mobilPhone;
    }
    public String getPhone(){
        return phone;
    }



    //Проверка пароля
    public boolean passwordIsTrue() {
        if (password.length() < 8 || password.length() > 16) {
            return false;
        } else {
            return true;
        }
    }


    //Проверка емейл
    public boolean emailIsTrue(){
        if (valueEmail()==64){
            return true;
        }else {
            return false;
        }
    }

    private int valueEmail() {
        for (int i = 0; i < this.email.length(); i++) {
            if (this.email.charAt(i) == 64) {            //индекс символа '@'=64
                return 64;
            }
        }
        return 0;
    }

}
