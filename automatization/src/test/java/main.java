import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class main {
    WebDriver driver;

    @BeforeClass
    public static void beforeClass(){
        final String path=String.format("%s/bin/chromedriver.exe",
                System.getProperty("user.dir"));
        System.setProperty("webdriver.chrome.driver",path);
    }



    @Before
    public void preconddition(){
        driver=new ChromeDriver();
//        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    };




    @Test
    public void userTest() throws InterruptedException {
        userClass user = new userClass();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String email = user.getEmail();
        String password = user.getPassword();
        String phone = user.getPhone();
        String mobilePhone = user.getMobilPhone();

        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys(user.getFirstName());
        driver.findElement(By.cssSelector("#last_name")).sendKeys(user.getLastName());
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys(user.getPhone());
        driver.findElement(By.cssSelector("#field_phone")).sendKeys(user.getMobilPhone());
        driver.findElement(By.cssSelector("#field_email")).sendKeys(user.getEmail());
        driver.findElement(By.cssSelector("#field_password")).sendKeys(user.getPassword());
        driver.findElement(By.cssSelector("#male")).click();
        driver.findElement(By.cssSelector("#position")).sendKeys(user.getPosition(), Keys.ENTER);
        driver.findElement(By.cssSelector(".create_account")).click();
//        Thread.sleep(2000);

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.alertIsPresent());

        Alert alert = driver.switchTo().alert();
        alert.accept();


        driver.navigate().to("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.cssSelector("#email")).sendKeys(user.getEmail());
        driver.findElement(By.cssSelector("#password")).sendKeys(user.getPassword());
        driver.findElement(By.cssSelector(".login_button")).click();
        Thread.sleep(3000);
        driver.findElement(By.linkText("Employees")).click();

        driver.findElement(By.cssSelector("#first_name")).sendKeys(user.getFirstName());
        driver.findElement(By.cssSelector("#last_name")).sendKeys(user.getLastName());
        driver.findElement(By.cssSelector("#position")).sendKeys(user.getPosition(), Keys.ENTER);
        driver.findElement(By.cssSelector("#mobile_phone")).sendKeys(user.getMobilPhone());
        driver.findElement(By.cssSelector("#gender")).sendKeys(user.getGender(), Keys.ENTER);
        driver.findElement(By.cssSelector("#search")).click();

        Thread.sleep(3000);

        String actualName=driver.findElement(By.xpath("//div/div[3]/table/tr/td[1]")).getText();
        final String expectedName=user.getFirstName();
        Assert.assertEquals("Wrong firstName",expectedName,actualName);
        System.out.println(actualName);
        System.out.println(expectedName);


        String actualLastName=driver.findElement(By.xpath("//div/div[3]/table/tr/td[2]")).getText();
        final String expectedLastName=user.getLastName();
        Assert.assertEquals("Wrong lastName",expectedLastName,actualLastName);
        System.out.println(actualLastName);
        System.out.println(expectedLastName);


        String actualEmai=driver.findElement(By.xpath("//div/div[3]/table/tr/td[3]")).getText();
        final String expectedEmail=user.getEmail();
        Assert.assertEquals("Wrong email",expectedEmail,actualEmai);
        System.out.println(actualEmai);
        System.out.println(expectedEmail);


        String actualMobilePhone=driver.findElement(By.xpath("//div/div[3]/table/tr/td[4]")).getText();
        final String expectedMobilePhone=user.getMobilPhone();
        Assert.assertEquals("Wrong mobilePhone",expectedMobilePhone,actualMobilePhone);
        System.out.println(actualMobilePhone);
        System.out.println(expectedMobilePhone);


        String actualPhone=driver.findElement(By.xpath("//div/div[3]/table/tr/td[5]")).getText();
        final String expectedPhone=user.getPhone();
        Assert.assertEquals("Wrong phone",expectedPhone,actualPhone);
        System.out.println(actualPhone);
        System.out.println(expectedPhone);


        String actualGender=driver.findElement(By.xpath("//div/div[3]/table/tr/td[6]")).getText();
        final String expectedGender=user.getGender();
        Assert.assertEquals("Wrong gender",expectedGender,actualGender);
        System.out.println(actualGender);
        System.out.println(expectedGender);


        String actualPosition=driver.findElement(By.xpath("//div/div[3]/table/tr/td[7]")).getText();
        final String expectedPosition=user.getPosition();
        Assert.assertEquals("Wrong position",expectedPosition,actualPosition);
        System.out.println(actualPosition);
        System.out.println(expectedPosition);
    }

    @After
    public  void postcondition(){
        driver.quit();
    };
}

