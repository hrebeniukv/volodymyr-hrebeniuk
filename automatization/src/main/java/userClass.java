import java.util.Random;

public class userClass {


    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String mobilPhone;
    private String phone;
    private String gender;
    private String position;


    {
        firstName="Ernest" ;
        lastName="Hemingway";
        email=generateRandomWord(4)+"@gmail.com";
        password="Mojaito"+new Random().nextInt(100);
        mobilPhone="38066"+new Random().nextInt(10000000);;
        phone="454216";
        gender="male";
        position="manager";
    }

    public String getFirstName(){
        return this.firstName; }
    public String getLastName(){
        return lastName;
    }
    public String getEmail(){
        return email;
    }
    public String getPassword(){
        return password;
    }
    public String getMobilPhone(){
        return mobilPhone;
    }
    public String getPhone(){
        return phone;
    }
    public String getGender(){
        return gender;
    }
    public String getPosition(){
        return position;
    }




    String generateRandomWord(int wordLength) {
        Random r = new Random(); // Intialize a Random Number Generator with SysTime as the seed
        StringBuilder sb = new StringBuilder(wordLength);
        for(int i = 0; i < wordLength; i++) { // For each letter in the word
            char tmp = (char) ('a' + r.nextInt('z' - 'a')); // Generate a letter between a and z
            sb.append(tmp); // Add it to the String
        }
        return sb.toString();}
}